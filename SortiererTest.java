import liste.*;

/**
 * Führt verschiedene Sortierverfahren mit einer Liste aus
 * 
 * @author Rainer Helfrich
 * @version Januar 2020
 */
public class SortiererTest
{
    /**
     * Die Liste, die gefüllt und sortiert werden soll
     */
    private Liste<Integer> liste;
    
    /**
     * Erzeugt eine neue Testklasse für die übergebene Liste
     * @param liste Die Liste, die gefüllt und sortiert werden soll
     */
    private SortiererTest(Liste<Integer> liste)
    {
        this.liste = liste;
    }
    
    /**
     * Führt die drei Sortierverfahren SelectionSort, InsertionSort und MergeSort mit der Liste aus
     * @param anzahl Die gewünschte Anzahl der Elemente
     */
    private void sortierTestAusfuehren(int anzahl)
    {
        if (liste == null)
        {
            System.out.println("Fehler: Keine Liste gesetzt.");
            return;
        }
        System.out.println("Listentyp: " + liste.getClass().getName());
        SortierenMitBenchmark smb = new SelectionSort(liste);
        smb.sortiereUndMesse(anzahl);
        smb = new InsertionSort(liste);
        smb.sortiereUndMesse(anzahl);
        smb = new MergeSort(liste);
        smb.sortiereUndMesse(anzahl);
    }
    
    /**
     * Erzeugt eine neue Liste und führt den Sortiertest aus.
     * @param anzahl Die gewünschte Anzahl der Elemente
     */
    public static void testDurchfuehren(int anzahl)
    {
        //# TODO: Ersetzen Sie "null" durch einen Konstruktoraufruf einer Implementation des ADTs Liste.
        Liste<Integer> l = null;
        
        SortiererTest st = new SortiererTest(l);
        st.sortierTestAusfuehren(anzahl);
    }
}
