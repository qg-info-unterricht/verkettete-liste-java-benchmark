import liste.*;

/**
 * Sortiert eine Liste nach dem SelectionSort-Algorithmus
 * 
 * @author Rainer Helfrich
 * @version Januar 2021
 */
public class SelectionSort extends SortierenMitBenchmark
{
    /**
     * Erzeugt eine neue Testklasse für die übergebene Liste
     * @param l Die Liste, die gefüllt und sortiert werden soll
     */
    public SelectionSort(Liste<Integer> l)
    {
        super(l);
    }

    /**
     * Gibt eine (ggf. neue) Liste zurück, die die Zahlen der Eingabeliste in sortierter Reihenfolge enthält
     * @param l Die zu sortierende Liste; wird u.U. verändert.
     * @return Eine Liste mit den sortierten Elementen der Eingabeliste
     */
    protected Liste<Integer> sortiere(Liste<Integer> l)
    {
        Class c = l.getClass();
        try
        {
            Liste<Integer> out = (Liste<Integer>)c.getConstructor().newInstance();
            while(!l.istLeer())
            {
                int minIndex = 0;
                int minValue = l.getNtenWert(0);
                int laenge = l.laenge();
                for (int i = 1; i < laenge; i++)
                {
                    int v = l.getNtenWert(i);
                    if (v < minValue)
                    {
                        minValue = v;
                        minIndex = i;
                    }
                }
                out.anhaengen(minValue);
                l.entferneBei(minIndex);
            }            
            return out;
        }
        catch(Exception x)
        {
            return null;
        }
    }
}
