import liste.*;

/**
 * Sortiert eine Liste nach dem InsertionSort-Algorithmus
 * 
 * @author Rainer Helfrich
 * @version Januar 2021
 */
public class InsertionSort extends SortierenMitBenchmark
{
    /**
     * Erzeugt eine neue Testklasse für die übergebene Liste
     * @param l Die Liste, die gefüllt und sortiert werden soll
     */
    public InsertionSort(Liste<Integer> l)
    {
        super(l);
    }

    /**
     * Gibt eine (ggf. neue) Liste zurück, die die Zahlen der Eingabeliste in sortierter Reihenfolge enthält
     * @param l Die zu sortierende Liste; wird u.U. verändert.
     * @return Eine Liste mit den sortierten Elementen der Eingabeliste
     */
    protected Liste<Integer> sortiere(Liste<Integer> l)
    {
        Class c = l.getClass();
        try
        {
            Liste<Integer> out = (Liste<Integer>)c.getConstructor().newInstance();
            while(!l.istLeer())
            {
                int value = l.getNtenWert(0);
                l.entferneBei(0);
                int index = 0;
                int laenge = out.laenge();
                while(index < laenge && out.getNtenWert(index) < value)
                {
                    index++;
                }
                out.einfuegenBei(index, value);                
            }            
            return out;
        }
        catch(Exception x)
        {
            return null;
        }
    }
}
