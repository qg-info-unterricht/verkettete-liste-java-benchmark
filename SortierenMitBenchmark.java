import liste.*;
import java.util.Random;

/**
 * Sortiert eine Liste und gibt die benötigte Zeit aus.
 * 
 * @author Rainer Helfrich
 * @version Januar 2021
 */
public abstract class SortierenMitBenchmark
{
    /**
     * Die Liste, die sortiert werden soll
     */
    private Liste<Integer> dieListe;

    /**
     * Erzeugt einen neuen Benchmark
     * @param l Die Liste, die sortiert werden soll
     */
    public SortierenMitBenchmark(Liste<Integer> l)
    {
        dieListe = l;
    }

    /**
     * Gibt eine (ggf. neue) Liste zurück, die die Zahlen der Eingabeliste in sortierter Reihenfolge enthält
     * @param l Die zu sortierende Liste; wird u.U. verändert.
     * @return Eine Liste mit den sortierten Elementen der Eingabeliste
     */
    protected abstract Liste<Integer> sortiere(Liste<Integer> l);

    /**
     * Füllt die Liste mit neuen Werten, sortiert sie und misst die für das Sortieren nötige Zeit.
     * Überprüft, ob die Sortierung korrekt war
     * Gibt die Anzahl der Elemente sowie den Zeitbedarf auf der Konsole aus
     * @param anzahl Die Anzahl der Elemente, die zufällig erzeugt werden sollen.
     */
    public void sortiereUndMesse(int anzahl)
    {
        while(!dieListe.istLeer())
        {
            dieListe.entferneBei(0);
        }
        Random r = new Random();
        for (int i = 0; i < anzahl; i++)
        {
            dieListe.anhaengen(r.nextInt(1000000));
        }
        long vorher = System.currentTimeMillis();
        dieListe = sortiere(dieListe);
        long nachher = System.currentTimeMillis();
        if (anzahl != dieListe.laenge())
        {
            System.out.println("Fehler: Die sortiere Liste hat nicht die richtige Länge!");
            return;
        }
        for (int i = 1; i < anzahl; i++)
        {
            if (dieListe.getNtenWert(i-1) > dieListe.getNtenWert(i))
            {
                System.out.println("Fehler bei Index " + i + ": Sortierung stimmt nicht.");
                return;
            }
        }
        System.out.println(getClass().getName() + ": " + anzahl + " Elemente, " + (nachher - vorher) + " ms");
    }
}
