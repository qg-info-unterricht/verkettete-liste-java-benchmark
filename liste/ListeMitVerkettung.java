package liste;

/**
 * Eine einfach verkettete Liste
 * 
 * @author Rainer Helfrich
 * @version (Oktober 2020)
 */
public abstract class ListeMitVerkettung<T> extends Liste<T>
{
    /**
     * Der erste Knoten der Liste
     */
    protected Listenknoten<T> anfang;

    /**
     * Erzeugt eine neue Liste
     */
    public ListeMitVerkettung()
    {
        anfang = null;
    }

    /**
     * Gibt zurück, ob die Liste leer ist.
     * @return true, wenn die Liste keine Elemente enthält; false sonst
     */
    public boolean istLeer()
    {
        return anfang == null;
    }
}
