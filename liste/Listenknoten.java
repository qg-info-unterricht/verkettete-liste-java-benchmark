package liste;

/**
 * Ein Knoten einer verketteten Liste
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public class Listenknoten<T>
{
    /**
     * Der Datenwert des Listenknotens
     */
    public T daten;

    /**
     * Der Nachfolger des Listenknotens
     */
    public Listenknoten<T> nachfolger;

    /**
     * Erzeugt einen neuen Listenknoten
     * 
     * @param daten Der Datenwert des Knotens
     * @param nachfolger Der Nachfolger des Knotens
     */
    public Listenknoten(T daten, Listenknoten<T> nachfolger)
    {
        this.daten = daten;
        this.nachfolger = nachfolger;
    }

}
