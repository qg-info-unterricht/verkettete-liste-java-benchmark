package liste;

/**
 * Der ADT Liste 
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public abstract class Liste<T>
{
    /**
     * Gibt die Anzahl der Elemente der Liste zurück
     */
    public abstract int laenge();

    /**
     * Gibt den n-ten Wert (0-basierte Zählweise) der Liste zurück.
     * @param n Der Index des gewünschten Elements
     * @return Den n-ten Wert
     */
    public abstract T getNtenWert(int n);

    /**
     * Hängt einen neuen Wert hinten an die Liste an.
     * @param val Der anzuhängende Wert
     */
    public abstract void anhaengen(T val);

    /**
     * Fügt einen neuen Wert an einer gewünschten Stelle in der Liste ein.
     * @param index Die Stelle, an der der neue Wert stehen soll (0 <= index <= laenge())
     * @param val Der einzufügende Wert
     */
    public abstract void einfuegenBei(int index, T val);

    /**
     * Gibt die Listenelemente hintereinander auf der Konsole aus.
     */
    public abstract void ausgeben();

    /**
     * Gibt zurück, ob ein Wert sich in der Liste befindet
     * @param val Der zu suchende Wert
     * @return true, wenn der Wert enthalten ist; false sonst
     */
    public abstract boolean enthaelt(T val);

    /**
     * Entfernt das Element, das am gegebenen Index steht, aus der Liste.
     * @param index Die Stelle, von der der Wert entfernt werden soll.
     */
    public abstract void entferneBei(int index);

    /**
     * Gibt zurück, ob die Liste leer ist.
     * @return true, wenn die Liste keine Elemente enthält; false sonst
     */
    public abstract boolean istLeer();
}
